'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var sass = require('gulp-sass');
var cleanCSS = require('gulp-clean-css');
var js_files = [
    'node_modules/owl.carousel/dist/owl.carousel.js',
    'node_modules/jquery-countdown/dist/jquery.countdown.js',
    'assets/js/**/*'
];
var css_files = [
    'node_modules/owl.carousel/dist/assets/owl.carousel.css',
    'node_modules/owl.carousel/dist/assets/owl.theme.default.css',
    'node_modules/animate.css/animate.min.css',
    'assets/scss/**/*'
];

gulp.task('default', ['css', 'js'], function() {
    // convert sass to clean minified css and include it
    gulp.watch('assets/scss/**/*', ['css']);
    gulp.watch('assets/js/*', ['js'])
});

gulp.task('build',['css', 'js']);

gulp.task('css', function() {
    gulp.src(css_files)
        .pipe(sass().on('error', sass.logError))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest('public_html/wp-content/themes/absoluteniche/assets/css/'));
});

gulp.task('js', function() {
   gulp.src(js_files)
       .pipe(concat('app.js'))
       .pipe(gulp.dest('public_html/wp-content/themes/absoluteniche/assets/js/'))
});
