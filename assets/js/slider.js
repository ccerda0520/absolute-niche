(function($) {
    $(".banners-carousel").owlCarousel({
        items: 1,
        animateOut: 'fadeOut',
        loop: true,
        touchDrag: false,
        pullDrag: false,
        mouseDrag: false,
        nav: false,
        dots: false,
        autoplay: true,
        autoplayTimeout: 10000,
        autoplayHoverPause: false
    });

    $(".banner-single").css('height', $(window).height());
    $(window).resize(function() {
        $(".banner-single").css('height', $(window).height());
    })
}(jQuery));