(function ($) {
    $(document).ready( function() {
        if($("body.single-product").length && $('#sale-countdown').length) {
            $('#sale-countdown').countdown($('#sale-countdown').data('time'), function(event) {
                $(this).html(event.strftime('%D days %H hrs %M min %S sec'));
            });
        }
    });
}(jQuery));











