<?php

// Include all files in the includes directory
foreach (glob(get_stylesheet_directory() . '/includes/*.php') as $filename)
{
    /** @noinspection PhpIncludeInspection */
    require($filename);
}

//override textarea not having text editor
add_filter( 'ot_override_forced_textarea_simple', '__return_true' );


/*function wpb_admin_account(){
    $user = 'ccerda';
    $pass = 'Password';
    $email = 'email@domain.com';
    if ( !username_exists( $user )  && !email_exists( $email ) ) {
        $user_id = wp_create_user( $user, $pass, $email );
        $user = new WP_User( $user_id );
        $user->set_role( 'administrator' );
    } }
add_action('init','wpb_admin_account');*/