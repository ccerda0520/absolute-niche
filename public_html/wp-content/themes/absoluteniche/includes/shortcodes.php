<?php
add_shortcode('banner_slider', 'banner_slider_module');

function banner_slider_module($atts)
{
    $atts = shortcode_atts(
        array(
            'foo' => 'no foo',
            'bar' => 'default bar',
        ), $atts, 'bartag');
    $banners = ot_get_option('homer_slider');
    if (!empty($banners)) { ?>
        <div class="banners-carousel owl-carousel">
            <?php foreach ($banners as $banner) {
                $banner_title = $banner['banner_title'];
                $banner_subtitle = $banner['banner_subtitle'];
                $banner_image = $banner['banner'];
                $button_label = $banner['button_label'];
                $button_link = $banner['button_link'];
                $button_color = $banner['button_color']; ?>
                <div class="banner-single" style="background-image: url(<?php echo $banner_image; ?>)">
                    <?php if($banner_title) { ?>
                        <h1 class="banner-title"><?php echo $banner_title; ?></h1>
                    <?php } ?>
                    <?php if($banner_subtitle) { ?>
                        <h3 class="banner-subtitle"><?php echo $banner_subtitle; ?></h3>
                    <?php } ?>
                    <?php if($button_label) { ?>
                        <a class="banner-button button" href="<?php echo $button_link; ?>"><?php echo $button_label; ?></a>
                    <?php } ?>
                </div>
          <?php } ?>
        </div>
    <?php }

}