<?php
define('BUILD_VERSION', 1.01);
add_action('wp_enqueue_scripts', 'add_scripts', 1000);

function add_scripts()
{
    $directory_css_files = get_stylesheet_directory() . '/assets/css/*.css';
    foreach( glob( $directory_css_files, GLOB_BRACE) as $file ) {
        // $file contains the name and extension of the file
        wp_enqueue_style( $file, get_stylesheet_directory_uri() . '/assets/css/' . basename($file), [], BUILD_VERSION);
    }
    wp_enqueue_script('app-js', get_stylesheet_directory_uri() . '/assets/js/app.js', ['jquery'], BUILD_VERSION, true);

}