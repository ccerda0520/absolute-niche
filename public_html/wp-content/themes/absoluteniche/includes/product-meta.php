<?php

function product_meta_boxes()
{
    $meta_boxes = array();
    $meta_boxes[] = array(
        'title' => 'Promotion Timer',
        'post_types' => 'product',
        'fields' => array(
            array(
                'name' => 'Turn on Timer?',
                'id' => 'timer-toggle',
                'type' => 'checkbox',
            ),
            array(
                'name' => 'End Time',
                'id' => 'end-time',
                'type' => 'datetime',
            ),
        ),
    );
    if (!class_exists('RW_Meta_Box'))
        return;

    foreach ($meta_boxes as $meta_box) {
        new RW_Meta_Box($meta_box);
    }

}

add_action('admin_init', 'product_meta_boxes');
