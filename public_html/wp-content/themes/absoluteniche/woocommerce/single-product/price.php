<?php


if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

global $product;

?>
<p class="price"><?php echo $product->get_price_html(); ?></p>
<?php if (!empty(get_post_meta($product->id, 'timer-toggle')[0]) && (new DateTime() < new DateTime(get_post_meta($product->id, 'end-time')[0]))) { ?>
    <div class="sale-timer">Sale ends in: <span id="sale-countdown" data-time="<?php echo get_post_meta($product->id, 'end-time')[0]; ?>"></span></div>
<?php } ?>

