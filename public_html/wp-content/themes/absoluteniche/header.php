<?php
/**
 * The header for our theme.
 *
 * @package shop-isle
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> <?php shop_isle_html_tag_schema(); ?>>
<head>

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
            new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
        })(window,document,'script','dataLayer','GTM-5QRW87L');</script>
    <!-- End Google Tag Manager -->

    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <?php if (is_singular() && pings_open(get_queried_object())) { ?>
        <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <?php } ?>

    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5QRW87L"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<?php do_action('shop_isle_before_header'); ?>

<!-- Preloader -->
<?php

/* Preloader */
if (is_front_page() && !is_customize_preview() && get_option('show_on_front') != 'page') :

    $shop_isle_disable_preloader = get_theme_mod('shop_isle_disable_preloader');

    if (isset($shop_isle_disable_preloader) && ($shop_isle_disable_preloader != 1)) :

        echo '<div class="page-loader">';
        echo '<div class="loader">' . __('Loading...', 'shop-isle') . '</div>';
        echo '</div>';

    endif;

endif;


?>

<?php do_action('shop_isle_header'); ?>

<?php do_action('shop_isle_after_header'); ?>
